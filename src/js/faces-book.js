$(document).ready(function() {

  const makeFace = function(name) {
      return $('<img>', {
        'class':'face',
        'title':name,
          'src':`img/2018/${name}.jpg`
       });
    };

  const names = [
    'Akshat',
    'Karthika',
    'Sanjana',
    'Mahidher',
    'Akhil',
    'Keerthana',
    'RVishnuPriya',
    'Anushree',
    'Mariah',
    'Aishu',
    'Rishi',
    'Jon',
    'Pavithran',
    'Sindhu',
    'Rishi',
    'Supriya',
    'Shruti',
    'Santhosh',
    'Supriya',
    'Gayatri',
    'Vishnu',
    'Varsha',
    'John',
    'Ameya',
    'Sudeep',
    'Janani',
    'Thamizh'
  ];

  const randomInt = function(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  };

  const shuffled = function(array) {
    const clone = array.slice(0);
    for (let i = 0; i < clone.length; i++) {
      const swap = randomInt(0, i);
      const temp = clone[swap];
      clone[swap] = clone[i];
      clone[i] = temp;
    }
    return clone;
  };

  shuffled(names).forEach(function(name) {
    $('faces').append(makeFace(name));
  });

  $('#shuffle').click(function() {
    window.location.reload();
  });

});
